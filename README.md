# CMakeIssueRepro

[clickable#370](https://gitlab.com/clickable/clickable/-/issues/370) repro attempt.

clone with `--recursive` or update submodules manually after a clone.

Start by building the library (`clickable build --libs`), then build the main project
with `clickable build`. This will fail with
```
  Could not find a package configuration file provided by "vmime" with any of
  the following names:

    vmimeConfig.cmake
    vmime-config.cmake

```
Next uncomment [CMakeLists.txt:57](https://gitlab.com/klhio/clickable-cmake-repro/-/blob/main/CMakeLists.txt#L57) and retry, this will fail with
```
  The imported target "vmime" references the file

     "/./libvmime.so.1.0.0"

  but this file does not exist.
```

## License

Copyright (C) 2022  Maciej Sopylo

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
